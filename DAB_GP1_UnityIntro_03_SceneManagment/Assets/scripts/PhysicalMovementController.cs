﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalMovementController : MonoBehaviour {

	[SerializeField] float m_movementSpeed = 100f;
	[SerializeField] float m_jumpSpeed = 10f;

	[SerializeField] float m_screenShakeSpeed = 10f;

	Rigidbody m_myRigidBody;

	Vector3 m_cameraStartPosition;
	bool m_doScreenShake = false;

	void Start () {
	
		///gets the rigid body component from the current game object
		m_myRigidBody = gameObject.GetComponent<Rigidbody> ();	

		if (m_myRigidBody == null) {

			Debug.Log ("No RigidBody found in PhysicalMovementController");
		}

		m_cameraStartPosition = Camera.main.gameObject.transform.localPosition;
	}

	// Update is called once per frame
	void Update () {
		
		///gets the horizontal axes if WASD or Arrow Keys were pressed
		///The value is -1 for left and +1 for right
		float horizontalMovement = Input.GetAxis ("Horizontal");
		float verticalMovement = Input.GetAxis ("Vertical");
	
		///we creating a direction vector for the force
		Vector3 movementForce = new Vector3();

		movementForce.x = horizontalMovement * m_movementSpeed * Time.deltaTime;
		movementForce.z = verticalMovement * m_movementSpeed * Time.deltaTime;

		///add y force when jumping
		if (Input.GetButton ("Jump")) { movementForce.y = m_jumpSpeed * Time.deltaTime;  }
			
		///and add tp force as an impulse to the rigid body
		m_myRigidBody.AddForce (movementForce,ForceMode.Impulse); 

		if (m_doScreenShake) 
		{
			Vector3 localPosition = Camera.main.gameObject.transform.localPosition;
			localPosition.x -= m_screenShakeSpeed * Time.deltaTime;

			if (localPosition.x <= m_cameraStartPosition.x) 
			{
				localPosition.x = m_cameraStartPosition.x;
				m_doScreenShake = false;
			}

			Camera.main.gameObject.transform.localPosition = localPosition;
		}
	}

	void OnCollisionEnter(Collision collision)
	{ 
		m_doScreenShake = true;
		Vector3 localPosition = Camera.main.gameObject.transform.localPosition;
		localPosition.x += 1f;
		Camera.main.gameObject.transform.localPosition = localPosition;
	}
}
